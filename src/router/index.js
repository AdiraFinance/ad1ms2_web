import Vue from 'vue'
import VueRouter from 'vue-router'
import VueSweetalert2 from 'vue-sweetalert2'
import vSelect from 'vue-select'

Vue.use(VueSweetalert2);
Vue.component('v-select', vSelect)

Vue.use(VueRouter)
const routes = [{
        path: '/dashboard',
        name: 'Home',
        component: () => import('../views/Home.vue')
    },
    // management user
    {
        path: '/management-user',
        name: 'ManagementUser',
        component: () => import('../views/ManagementUser/ListUser.vue')
    },
    {
        path: '/add-user',
        name: 'AddUser',
        component: () => import('../views/ManagementUser/AddUser.vue')
    },
    {
        path: '/edit-user/:id',
        name: 'EditUser',
        component: () => import('../views/ManagementUser/EditUser.vue')
    },
    {
        path: '/user-matrix/:id',
        name: 'UserMatrix',
        component: () => import('../views/ManagementUser/UserMatrix.vue')
    },
    // end management user

    // management parameter
    {
        path: '/management-parameter',
        name: 'ManagementParameter',
        component: () => import('../views/ManagementParameter/ListParameter.vue')
    },
    {
        path: '/add-parameter',
        name: 'AddParameter',
        component: () => import('../views/ManagementParameter/AddParameter.vue')
    },
    {
        path: '/edit-parameter/:id',
        name: 'EditParameter',
        component: () => import('../views/ManagementParameter/EditParameter.vue')
    },
    // end management parameter

    // management form
    {
        path: '/dedup-individu',
        name: 'DedupIndividu',
        component: () => import('../views/ManagementForm/DedupIndividu.vue')
    },
    {
        path: '/dedup-company',
        name: 'DedupCompany',
        component: () => import('../views/ManagementForm/DedupCompany.vue')
    },
    {
        path: '/form-m-individu',
        name: 'FormMIndividu',
        component: () => import('../views/ManagementForm/FormMIndividu.vue')
    },
    {
        path: '/form-m-company',
        name: 'FormMCompany',
        component: () => import('../views/ManagementForm/FormMCompany.vue')
    },
    {
        path: '/form-reguler-survey',
        name: 'FormRegulerSurvey',
        component: () => import('../views/ManagementForm/FormRegulerSurvey.vue')
    },
    {
        path: '/form-pac-ia-aos',
        name: 'FormPACIAAOS',
        component: () => import('../views/ManagementForm/FormPACIAAOS.vue')
    },
    {
        path: '/form-pac-nds',
        name: 'FormPACNDS',
        component: () => import('../views/ManagementForm/FormPACNDS.vue')
    },
    {
        path: '/form-app',
        name: 'FormApp',
        component: () => import('../views/ManagementForm/FormApp.vue')
    },
    {
        path: '/form-ia',
        name: 'FormIA',
        component: () => import('../views/ManagementForm/FormIA.vue')
    },
    {
        path: '/form-aos',
        name: 'FormAOS',
        component: () => import('../views/ManagementForm/FormAOS.vue')
    },
    {
        path: '/form-survey',
        name: 'FormSurvey',
        component: () => import('../views/ManagementForm/FormSurvey.vue')
    },
    // end management form
    // start ilinksys
    {
        path: '/ilinksys-form',
        name: 'ilinksysform',
        component: () => import('../views/Ilinksys/ilinksys.vue')
    },
    // end ilinksys
    // start ibooking
    {
        path: '/ibooking-form',
        name: 'ibookingform',
        component: () => import('../views/Ibooking/ibooking.vue')
    },
    // end ibooking
    //start employee
    {
        path: '/form-mapping-employee',
        name: 'formmappingemployee',
        component: () => import('../views/Employeee/mapping-employee.vue')
    }, {
        path: '/form-dedicated-employee',
        name: 'formdedicatedgemployee',
        component: () => import('../views/Employeee/dedicated-employee.vue')
    }, {
        path: '/form-employee-zona',
        name: 'formemployeezona',
        component: () => import('../views/Employeee/zona-employee.vue')
    },

    {
        path: '/form-employee-overtime',
        name: 'formemployeeovertime',
        component: () => import('../views/Employeee/overtime-employee.vue')
    },

    {
        path: '/form-employee-offwork',
        name: 'formemployeeoffwork',
        component: () => import('../views/Employeee/offwork-employee.vue')
    }, {
        path: '/form-holiday-branch',
        name: 'formholidaybranch',
        component: () => import('../views/Employeee/holiday-branch.vue')
    },

    //end employee
    {
        path: '/add-field',
        name: 'AddField',
        component: () => import('../views/ManagementForm/Form/AddField.vue')
    },
    {
        path: '/',
        name: 'Login',
        component: () => import('../views/Login.vue')
    }
]

const router = new VueRouter({
    routes
})

export default router
