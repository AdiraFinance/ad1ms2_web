$(document).ready(function() {

    // Switch Status
    $('.status').change(function () {
        if ($(this).is(':checked')) {
            $(this).closest('.row').find('.control-label').addClass('active');
        } else {
            $(this).closest('.row').find('.control-label').removeClass('active');
        }
    });

    // Datepicker
    $(function () {
        $(".datepicker").datepicker();
    });

});